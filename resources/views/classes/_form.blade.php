<div class="mb-4">
    <label class="block" for="task">Class</label>
    <input class="focus" type="text" name="name" value="{{ old('name', isset($sclClass) ? $sclClass->name : '')}}" id="name">
    <p class=" text-red-500">{{$errors->first("name")}}</p>
</div>
<div>
    <a class="p-1 pl-3 pr-3 bg-transparent border-2 border-gray-500 text-gray-500 text-lg rounded-lg hover:bg-gray-500 hover:text-gray-100 focus:border-4 focus:border-gray-300" href="{{ route('schoolclasses.index')}}">Back</a>

    <button class="p-1 pl-7 pr-7 bg-transparent border-2 border-green-500 text-green-500 text-lg rounded-lg hover:bg-green-500 hover:text-gray-100 focus:border-4 focus:border-green-300" type="submit">{{$buttonText}}</button>
</div>
