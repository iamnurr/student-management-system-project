<div class="mb-4">
    <label class="block" for="category">Class</label>
    <select name="class_id" id="class_id" >
        <option value="">{{ __("Select Class")}}</option>
        @foreach ($classes as $id => $name)
            <option value="{{ $id }}" >
                {{ $name }}
            </option>
        @endforeach
    </select>
    <p class=" text-red-500">{{$errors->first("class_id")}}</p>
</div>
<div class="mb-4">
    <label class="block" for="task">Student Name</label>
    <input class="focus" type="text" name="name" value="{{ old('name', isset($subject) ? $subject->name : '')}}" id="name">
    <p class=" text-red-500">{{$errors->first("name")}}</p>
</div>
<div class="mb-4">
    <label class="block" for="task">Student Email</label>
    <input class="focus" type="text" name="email" value="{{ old('email', isset($subject) ? $subject->email : '')}}" id="name">
    <p class=" text-red-500">{{$errors->first("email")}}</p>
</div>
<div class="mb-4">
    <label class="block" for="task">Student Roll</label>
    <input class="focus" type="number" name="roll" value="{{ old('roll', isset($subject) ? $subject->roll : '')}}" id="name">
    <p class=" text-red-500">{{$errors->first("roll")}}</p>
</div>
<div>
    <a class="p-1 pl-3 pr-3 bg-transparent border-2 border-gray-500 text-gray-500 text-lg rounded-lg hover:bg-gray-500 hover:text-gray-100 focus:border-4 focus:border-gray-300" href="{{ route('students.index')}}">Back</a>

    <button class="p-1 pl-7 pr-7 bg-transparent border-2 border-green-500 text-green-500 text-lg rounded-lg hover:bg-green-500 hover:text-gray-100 focus:border-4 focus:border-green-300" type="submit">{{$buttonText}}</button>
</div>
