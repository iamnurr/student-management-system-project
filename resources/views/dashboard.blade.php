<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @forelse ($classes as $class)
                    <a href="{{route('classResult',$class->id)}}" class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm">{{$class->name}}</a>
                    @empty
                        <tr>
                            <td colspan="4" class="px-4 py-3 text-center">{{ __("No class found.")}}</td>
                        </tr>
                    @endforelse
                    @if ($results != null)
                    <table class="w-full mt-4">
                        <thead>
                          <tr class="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
                            <th class="px-4 py-3">ID</th>
                            <th class="px-4 py-3">Name</th>
                            <th class="px-4 py-3">Total Mark</th>
                          </tr>
                        </thead>
                        <tbody class="bg-white">
                          @forelse ($results as $result)
                          <tr class="text-gray-700">
                            <td class="px-4 py-3 border">{{ $result->id}}</td>
                            <td class="px-4 py-3 text-ms font-semibold border"><a href="{{route('single',$result->student->id)}}" class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm">{{$result->student->name}}</a></td>

                            <td class="px-4 py-3 text-ms font-semibold border">{{$result->marks}}</td>
                          </tr>
                          @empty
                          <tr>
                            <td colspan="4" class="px-4 py-3 text-center">{{ __("No result found.")}}</td>
                        </tr>
                          @endforelse
                        </tbody>
                      </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
