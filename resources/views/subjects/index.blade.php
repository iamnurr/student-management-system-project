<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Subjects') }}
            </h2>
            @if ($clsSubjects != null)
                <h2 class="px-10 py-1 font-semibold leading-tight text-gray-700 bg-gray-100 rounded-sm">{{$clsSubjects->name}} subjects list</h2>
            @endif
            <a class="px-2 py-1 font-semibold leading-tight text-blue-700 bg-blue-100 rounded-sm" href="{{ route('subjects.create')}}">Create Subject</a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @forelse ($classes as $class)
                    <a href="{{route('classSubjects',$class->id)}}" class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm">{{$class->name}}</a>
                    @empty
                        <tr>
                            <td colspan="4" class="px-4 py-3 text-center">{{ __("No class found.")}}</td>
                        </tr>
                    @endforelse

                    @if ($clsSubjects != null)
                    {{-- <h2 class="font-semibold text-xxl text-gray-800 leading-tight">{{$clsSubjects->name}}</h2> --}}
                    <table class="w-full mt-4">
                        <thead>
                          <tr class="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
                            <th class="px-4 py-3">ID</th>
                            <th class="px-4 py-3">Subject </th>
                            <th class="px-4 py-3">Action</th>
                          </tr>
                        </thead>
                        <tbody class="bg-white">

                          @forelse ($clsSubjects->subjects as $clsSubject)
                          <tr class="text-gray-700">
                            <td class="px-4 py-3 border">{{ $clsSubject->id}}</td>
                            <td class="px-4 py-3 text-ms font-semibold border">{{$clsSubject->subject}}</td>
                            <td class="py-3 px-4 border">
                                <a href="{{route('subjects.edit',$clsSubject->id)}}" class="px-2 py-1 font-semibold leading-tight text-yellow-700 bg-yellow-100 rounded-sm">Edit</a>
                                <a href="{{route('subjects.destroy', $clsSubject->id)}}" class="delete-row px-2 py-1 font-semibold leading-tight text-red-700 bg-red-100 rounded-sm" data-confirm="Are You Sure To Delete This?"> Delete </a>

                            </td>
                          </tr>
                          @empty
                          <tr>
                            <td colspan="4" class="px-4 py-3 text-center">{{ __("No subject found.")}}</td>
                        </tr>
                          @endforelse
                        </tbody>
                      </table>
                    @endif
                      <div class="my-4">
                           {{-- {{$categories->links()}} --}}
                      </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
