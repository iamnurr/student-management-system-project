<div class="mb-4">
    <label class="block" for="category">Class</label>
    <select name="class_id" id="class_id" >
        <option value="">{{ __("Select Class")}}</option>
        @foreach ($classes as $id => $name)
            <option value="{{ $id }}" >
                {{ $name }}
            </option>
        @endforeach
    </select>
    <p class=" text-red-500">{{$errors->first("class_id")}}</p>
</div>
<div class="mb-4">
    <label class="block" for="category">Student</label>
    <select name="student_id" id="student_id" >
        <option value="">{{ __("Select student")}}</option>

    </select>
    <p class=" text-red-500">{{$errors->first("student_id")}}</p>
</div>

@forelse ($subjects as $id => $subject)
<div class="mb-4">
    <label class="block" for="task" >{{ $subject }}</label>
    <input class="focus" type="hidden"  name="subject_id[]" value="{{ $id}}" id="subject_id">
    <input class="focus" type="text"  name="mark[]" id="mark">
    <p class=" text-red-500">{{$errors->first("mark.*")}}</p>
</div>
@empty

@endforelse
<div>
    <a class="p-1 pl-3 pr-3 bg-transparent border-2 border-gray-500 text-gray-500 text-lg rounded-lg hover:bg-gray-500 hover:text-gray-100 focus:border-4 focus:border-gray-300" href="{{ route('results.index')}}">Back</a>

    <button class="p-1 pl-7 pr-7 bg-transparent border-2 border-green-500 text-green-500 text-lg rounded-lg hover:bg-green-500 hover:text-gray-100 focus:border-4 focus:border-green-300" type="submit">{{$buttonText}}</button>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('select[name="class_id"]').on('change', function() {
            var classID = jQuery(this).val();
            if (classID) {
                jQuery.ajax({
                    // url: '/getclassid/' + classID,
                    url: "/getclassid/" + classID,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        jQuery('select[name="student_id"]').empty();
                        jQuery('select[name="student_id"]').append(
                            '<option>Select Students</option>');
                        jQuery.each(data, function(key, value) {
                            $('select[name="student_id"]').append('<option value="' + key +
                                '">' + value + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="student_id"]').empty();
                $('select[name="student_id"]').append('<option>No Students Found</option>');
            }
        });
    });
    </script>
