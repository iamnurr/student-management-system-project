<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Student Result') }}
            </h2>
            <a class="px-2 py-1 font-semibold leading-tight text-blue-700 bg-blue-100 rounded-sm" href="{{ route('classResult', $student->class->id)}}">All Result</a>

        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-2xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="w-full">
                        <tr class="text-gray-700">
                            <td class="py-3 px-4 ">
                                Class: {{$student->class->name}}
                            </td>
                        </tr>
                        <tr class="text-gray-700">
                            <td class="py-3 px-4 ">
                                Name: {{$student->name}}
                            </td>
                        </tr>
                        <tr class="text-gray-700">
                            <td class="py-3 px-4 ">
                                Email: {{$student->email}}
                            </td>
                        </tr>
                        @foreach ($results as $result)
                        <tr class="text-gray-700">
                            <td class="py-3 px-4 ">
                                {{$result->subjects->subject}}: {{$result->mark}}
                            </td>
                        </tr>
                        @endforeach
                        <tr class="text-blue-700">
                            <td class="py-3 px-4">Total Mark: {{ $totalMark }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
