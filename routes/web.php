<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\ResultImportController;
use App\Http\Controllers\SchoolClassController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentImportController;
use App\Http\Controllers\SubjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
    Route::get('/dashboard/{id}/class', [DashboardController::class, 'classResult'])->name('classResult');

    Route::resource('schoolclasses', SchoolClassController::class);

    Route::resource('subjects', SubjectController::class);
    Route::get('subjects/{id}/class', [SubjectController::class, 'classSubjects'])->name('classSubjects');

    Route::resource('students', StudentController::class);
    Route::get('students/{id}/class', [StudentController::class, 'classStudents'])->name('classStudents');

    Route::resource('results', ResultController::class);

    Route::get('result/{id}/show', [ResultController::class, 'singleResult'])->name('single');

    Route::get('student/exl-import', [StudentImportController::class, 'create'])->name('exlImport');
    Route::post('student/exl-store', [StudentImportController::class, 'store'])->name('exlStore');

    Route::get('result/exl-import', [ResultImportController::class, 'create'])->name('resultImport');
    Route::post('result/exl-store', [ResultImportController::class, 'store'])->name('resultStore');

    Route::get('getclassid/{id}',[ResultController::class, 'getClassId']);
});

require __DIR__.'/auth.php';
