<?php

namespace App\Http\Controllers;

use App\Models\Result;
use App\Models\School_class;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard () {
        $data['classes'] = School_class::all();
        $data['results'] = null;

        return view('dashboard', $data);
    }

    public function classResult ($id)
    {
        //dd($id);
        $data['classes'] = School_class::all();
        $data['results'] = Result::with('student')
        ->where('class_id', $id)
        ->groupBy('student_id')
        ->selectRaw('sum(mark) as marks, student_id,id,class_id')
        ->orderByRaw('SUM(mark) DESC')
        ->get();
//dd($data);
        return view('dashboard', $data);
    }
}
