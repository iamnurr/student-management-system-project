<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Requests\SubjectRequest;
use App\Models\School_class;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data['subjects'] = Subject::with('classes')->get();
        //dd($data);
        $data['classes'] = School_class::all();
        $data['clsSubjects'] = null;
        return view('subjects.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['classes'] = School_class::pluck('name','id');
        return view('subjects.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    {
        //dd($request->input());
        $subject = Subject::create([
            'subject' => $request->get('subject'),
        ]);

        if (empty($subject)) {
            return redirect()->back()->withInput();
        }
        $subject->classes()->attach($request->get('class_ids'));

        return redirect()
        ->route('subjects.index')
        ->with('SUCCESS', __('Class has been created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        return redirect()->back()->with('ERROR', __('Nothing to show.'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        $data['subject'] = $subject;
        $data['classes'] = School_class::pluck('name','id');
        //dd($data);
        return view('subjects.edit', $data);
        //return redirect()->back()->with('ERROR', __('Edit not allowed.'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {   //dd($request->input());
        $subject->subject = $request->get('subject');


        if($subject->update()){
            $subject->classes()->sync($request->get('class_ids'));
            return redirect()->route('subjects.index')->with('SUCCESS', __('Subject has been Updated.'));
        }
        return redirect()->back()->withInput()->with('ERROR', __('Fail to Updated.'));
        //return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        if($subject->classes()->detach()){
            $subject->delete();
            return redirect()->back()->with('SUCCESS', __('Subject has been deleted.'));
        }
        return redirect()->back()->with('ERROR', __('Please try again.'));

    }

    public function classSubjects ($id)
    {
        $data['classes'] = School_class::all();
        $data['clsSubjects'] = School_class::with('subjects')->where('id', $id)->first();

        return view('subjects.index',$data);
    }
}
