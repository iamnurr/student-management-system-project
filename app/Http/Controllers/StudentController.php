<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\School_class;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['classes'] = School_class::all();
        $data['students'] = null;

        return view('students.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['classes'] = School_class::pluck('name','id');

        return view('students.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $student = Student::create([
            'roll' => $request->get('roll'),
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'class_id' => $request->get('class_id'),
        ]);

        if(empty($student)){
            return redirect()->back()->withInput();
        }

        return redirect()
        ->route('students.index')
        ->with('SUCCESS', __('Student has been created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    public function classStudents ($id)
    {
        $data['classes'] = School_class::all();
        $data['students'] = Student::with('class')->where('class_id', $id)->get();

        return view('students.index',$data);
    }
}
