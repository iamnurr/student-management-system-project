<?php

namespace App\Http\Controllers;

use App\Http\Requests\SclClassRequest;
use App\Models\School_class;
use Illuminate\Http\Request;

class SchoolClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['classes'] = School_class::all();
        return view('classes.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SclClassRequest $request)
    {
        $sclClass = School_class::create([
            'name' => $request->get('name'),
        ]);

        if (empty($sclClass)) {
            return redirect()->back()->withInput();
        }

        return redirect()
        ->route('schoolclasses.index')
        ->with('SUCCESS', __('Class has been created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\School_class  $school_class
     * @return \Illuminate\Http\Response
     */
    public function show(School_class $school_class)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\School_class  $school_class
     * @return \Illuminate\Http\Response
     */
    public function edit(School_class $schoolclass)
    {
        //dd($schoolclass);
        $data['sclClass'] = $schoolclass;
        return view('classes.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\School_class  $school_class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School_class $schoolclass)
    {
        $schoolclass->name = $request->get('name');

        if($schoolclass->update()){
        return redirect()->route('schoolclasses.index')->with('SUCCESS', __('Class has been Updated.'));
        }
        return redirect()->back()->withInput()->with('ERROR', __('Fail to Updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\School_class  $school_class
     * @return \Illuminate\Http\Response
     */
    public function destroy(School_class $schoolclass)
    {
        if($schoolclass->delete()){
            return redirect()->back()->with('SUCCESS', __('Class has been deleted.'));
        }
        return redirect()->back()->with('ERROR', __('Please try again.'));
    }
}
