<?php

namespace App\Http\Controllers;

use App\Models\Result;
use App\Models\Student;
use App\Models\Subject;
use App\Models\School_class;
use Illuminate\Http\Request;
use App\Http\Requests\ResultRequest;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['results'] = Result::with('student','sclClass')
        ->groupBy('student_id')
        ->selectRaw('sum(mark) as marks, student_id,id,class_id')
        ->orderByRaw('SUM(mark) DESC')
        ->get();

        return view('results.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['classes'] = School_class::pluck('name','id');
        $data['students'] = Student::pluck('name','id');
        $data['subjects'] = Subject::pluck('subject','id');

        //dd($data);

        return view('results.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResultRequest $request)
    {
        $result = Result::where('student_id', $request->get('student_id'))
        ->where('subject_id', $request->get('subject_id'))->first();

        if ($result) {
        return redirect()->back()->withInput()->with('ERROR', __('Result already added.'));
        }

        $subject = $request->get('subject_id');
        $mark = $request->get('mark');

        for($i=0; $i<count($subject);$i++){
            $data = [
                'class_id' => $request->get('class_id'),
                'student_id' => $request->get('student_id'),
                'subject_id' => $subject[$i],
                'mark' => $mark[$i],
            ];
            $create = Result::create($data);
        }

        if(empty($create)) {
            return redirect()->back()->withInput()->with("ERROR", __("Pleace input again"));
        }
        return redirect()->route('results.index')->with("SUCCESS", __("Marks Added successfully"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        //dd($result);
        // $data['results'] = Result::where('class_id', $result->class_id)
        // ->with('student')
        // //->groupBy('student_id')
        // ->selectRaw('sum(mark) as marks, student_id,id,class_id')
        // ->orderByRaw('SUM(mark) DESC')->get();
        // //dd($data);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        return redirect()->back()->with('ERROR', __('Do not try this.'));

    }

    public function singleResult ($id)
    {
        $data['student'] = Student::where('id', $id)->first();
        $data['results'] = Result::with('subjects')->where('student_id', $id)->get();
        $data['totalMark'] = 0;
        foreach ($data['results'] as $result) {
            $data['totalMark'] +=  $result['mark'];
        }
        //dd($data['totalMark']);
        return view('results.single', $data);
    }

    public function getClassId($id) {

        $studentClass = Student::where('class_id', $id)->pluck('name','id');
        // dd($studentClass);

        // return json_encode($studentClass);
        return response()->json($studentClass);
    }

}
