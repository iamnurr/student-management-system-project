<?php

namespace App\Http\Controllers;

use App\Imports\StudentsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StudentImportController extends Controller
{
    public function create ()
    {
        return view('exl.student.create');
    }

    public function store (Request $request)
    {
        $request->validate([
            'file' => ['required','mimes:xlsx'],
        ]);

        $file = $request->file('file');
        Excel::import(new StudentsImport, $file);

        return redirect()
        ->route('students.index')
        ->with('SUCCESS', __('Student has been imported.'));
    }
}
