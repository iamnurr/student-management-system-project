<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Maatwebsite\Excel\Excel;
use App\Imports\ResultImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class ResultImportController extends Controller
{
    public function create ()
    {
        return view('exl.result.create');
    }

    public function store (Request $request)
    {
        $request->validate([
            'file' => ['required','mimes:xlsx'],
        ]);

        Excel::import(new ResultImport,request()->file('file'));

        return redirect()
        ->route('results.index')
        ->with('SUCCESS', __('result has been imported.'));
    }
}
