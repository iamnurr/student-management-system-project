<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SclClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('school_classes')->ignore($this->route('schoolclass')) ,
                'min:5',
                'max:255',
            ],

        ];
    }

    public function messages()
    {
        return [

            'name.required' => 'Name Required.',
            'name.min' => 'Minimum 5 letter.',
        ];
    }
}
