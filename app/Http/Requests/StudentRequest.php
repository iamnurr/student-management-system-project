<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                Rule::unique('students')->ignore($this->route('student')),
            ],
            'name' => [
                'required',
                'min:5',
                'max:255',
            ],
            'class_id' => [
                'required',
            ],
            'roll' => [
                'required',
                Rule::unique('students')->ignore($this->route('student')),
            ],
        ];
    }

    public function messages()
    {
        return [

            'email.required' => 'Email Required.',
            'class_id.required' => 'Class name Required.',
            'name.min' => 'Minimum 5 letter.',
        ];
    }
}
