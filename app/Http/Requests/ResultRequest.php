<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_id' => [
                'required',
            ],
            'student_id' => [
                'required',
            ],
            'subject_id' => [
                'required',
            ],
            'mark.*' => [
                'required',
            ],
        ];
    }

    public function messages()
    {
        return [

            'class_id.required' => 'Class Required.',
            'student_id.required' => 'Student Required.',
            'subject_id.required' => 'Subject Required.',
            'mark.*.required' => 'Mark Required.',
        ];
    }
}
