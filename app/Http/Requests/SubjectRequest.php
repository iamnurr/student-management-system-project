<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => [
                'required',
                Rule::unique('subjects')->ignore($this->route('subject')) ,
                'min:3',
                'max:255',
            ],
            'class_ids' => [
                'required',
            ]
        ];
    }

    public function messages()
    {
        return [

            'subject.required' => 'Subject Required.',
            'subject.min' => 'Minimum 5 letter.',
        ];
    }
}
