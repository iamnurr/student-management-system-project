<?php

namespace App\Imports;

use App\Models\School_class;
use App\Models\Student;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentsImport implements ToCollection, WithHeadingRow
{

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        //dd($rows);
        Validator::make($rows->toArray(), [
            '*.class' => ['required','string'],
            '*.name' => ['required','string'],
            '*.email' => ['required','string'],
            '*.roll' => ['required','numeric','unique:students'],
        ])->validate();

        foreach ($rows as $row) {
            //dd($row['class']);
            $sclClass = School_class::where('name', $row['class'])->first();
            //dd($sclClass);
            if ($sclClass != null) {
                Student::firstOrCreate([
                    'class_id' => $sclClass->id,
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'roll' => $row['roll'],
                ]);
            }
        }
    }

}
