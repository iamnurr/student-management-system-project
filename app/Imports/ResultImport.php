<?php

namespace App\Imports;

use App\Models\Result;
use App\Models\School_class;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ResultImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.student' => ['required','string'],
            '*.roll' => ['required','numeric'],
        ])->validate();

        foreach ($rows as $row)
        {
            $student = Student::where('roll', $row['roll'])->first();
            if ($student != null) {
                $result = Result::where('student_id', $student->id)->first();
                if ($result === null) {
                    foreach($row as $colum => $value)
                    {
                        if ($colum != 'student' && $colum != 'roll') {
                            $subject = Subject::where('subject', $colum)->first();
                            if ($subject != null) {
                                Result::create([
                                    'mark' => $value,
                                    'class_id' => $student->class_id,
                                    'student_id' => $student->id,
                                    'subject_id' => $subject->id,
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }
}
