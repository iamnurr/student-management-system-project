<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'roll',
        'class_id',
        'name',
        'email',
    ];

    public function class ()
    {
        return $this->hasOne(School_class::class, 'id','class_id');
    }
}
