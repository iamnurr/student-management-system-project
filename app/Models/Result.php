<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    protected $fillable = [
        'mark',
        'class_id',
        'student_id',
        'subject_id',
    ];

    public function student() {

        return $this->belongsTo(Student::class, 'student_id');
    }

    public function subjects() {

        return $this->belongsTo(Subject::class, "subject_id","id");
    }

    public function sclClass() {

        return $this->belongsTo(School_class::class,"class_id","id");
    }

}
