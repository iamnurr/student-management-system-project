<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolclassSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_class_subject', function (Blueprint $table) {
            $table->unsignedBigInteger('school_class_id');
            $table->unsignedBigInteger('subject_id');

            $table->unique(['subject_id','school_class_id']);

            $table->foreign('school_class_id')
            ->references('id')
            ->on('school_classes')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('subject_id')
            ->references('id')
            ->on('subjects')
            ->onDelete('restrict')
            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_class_subject');
    }
}
